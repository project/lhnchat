LiveHelpNow

LiveHelpNow is a responsive, reliable, and extremely affordable Help Desk 
Software for your website. The live chat button plugin adds click-to-chat 
functionality to your website, allowing you to interact directly with 
website visitors the same way you would in a traditional retail store. You 
wouldn't ignore customers in your store, so don't ignore the ones visiting 
your site every day!

Chat with customers using any computer or mobile device and monitor website 
visitors in real-time using our Top Rated Chat System. Invite visitors to 
chat automatically or proactively, help them with product selection, and 
increase sales! When you're unavailable the chat button plugin switches to an 
'offline mode' that allows visitors to leave you messages.

This plugin is exclusively for the LiveHelpNow Chat System & also integrates 
seamlessly with:
- LiveHelpNow Ticket System
- LiveHelpNow Callback Request System

Ticket System - manage customer inquiries from start to finish; avoid the 
chaos and pitfalls of email.

LiveHelpNow Ticket System is an advanced inquiry management system that provides 
your website with its own helpdesk solution. Instead of sending you a string of 
emails your customers can submit their inquiries directly into the Ticket system 
using a customizable web form & then get instant confirmation that their request 
has been received. Automated status notifications keep customers in the loop and 
analytical reports gauge the effectiveness of your support team. The Ticket 
system allows your store to be open even when you're not there, giving you the 
ability to provide customer service 24/7/365.

Callback Request System - a missed phone call can very easily be a lost sale!

You wouldn't leave your business telephone without an answering machine; now 
there's no reason to leave visitors to your website without a way to leave a 
message for you. Our 24/7 Callback Request system allows your customers to 
notify you when they need to speak with a live operator. Collect the person's 
contact information and the time at which they'd like you to call them on 
a customizable web form and assign callbacks to your staff; requests are then 
queued and for easy and straightforward management.

Full Feature List:

Live Chat System:
- Install anywhere! Any website and any platform!
- Login from anywhere! Web, Windows, Mac...
- Chat window is compatible with all browsers, no exceptions and no limitations!
- 2 step installation: Copy the code to your website, Install Alerter 
  notification software
- Full real-time visitor monitoring, find out where your visitors are from, how 
  they found your site, how long they stay on your website, which page the 
  visitor are on... ALL IN REAL TIME
- Use your internet enabled mobile device to chat with your customers like 
  iPhone, Windows Mobile, Blackberry, Google Phone, Palm, etc. Apps for 
  iOS and Android devices are also available.
- Use LiveHelpNow live chat system on unlimited websites and pages, there are 
  no limitations.
- Customize as many chat windows as you need and link them all to one account!
- Huge libraries of Live Chat buttons provided, or use your own.
- Spectrum of proactive features, invite browsing visitors to live chat either 
  automatically or manually
- Everything is customizable: Chat windows, Messages, Invitations, Live Chat 
  Buttons, Flows, Business rules, etc.
- Fully secured with financial grade 256-bit encryption
- Supports multiple operators, and filled with features for real-time operator 
  monitoring.
- Multiple Simultaneous Chat Sessions, one operator can chat with an unlimited 
  number of customers at the same time with no stress!
- Push pages to customers while in chat
- Transfer chat sessions to any other online operator on the account
- Operator to Operator messaging
- Patent pending "Whisper" Technology allows operators to coach each other! Send 
  private messages into a chat session assigned to another operator. This 
  message will be visible only to the operator who this chat session is assigned 
  to and not the customer.
- Prospect and returning visitor detection and visitor flagging.
- Dynamic billboards. Advertise to your customers in the chat window, up to 10 
  rotating advertising messages are allowed!
- Save frequently used messages, links and images as Canned Responses for 
  "1-click send" in chat
- Spell Check
- Support for ALL languages, with no limitations.
- Review each customer's previous chat history
- Ability to add notes to visitors and make them available to other operators 
  should the visitor come back in the future.
- Ban visitors who cause trouble to prevent them from chatting and wasting 
  valuable company time.
- Offline window allows leaving a message when everyone's offline
- Print/Email chat transcripts
- Visitor "Typing" notification
- Custom information feed - ability to pass information from your website to 
  your operators
- 24/7 support 877-LIVE-001
- Free hands on support with installation
- Free live demo
- On-demand custom integration with other CRM systems
- and much more...

Ticket System:
- Email-to-ticket (Email Piping) integration!
- Full ticket lifecycle from opening to resolution
- Easy to understand and use, straightforward interface to manage customer 
  inquiries
- Customers are able to easily submit new tickets and track their progress 
  on the web
- Easy 1 minute installation on your website
- Track and measure the effectiveness of your staff
- Create Knowledge base articles in your Support System based on resolved 
  tickets with a click of a button
- Improve communication with your customers with automated responses and 
  ticket status notifications
- Fully customizable, with no limitations. All screens, labels, information 
  fields, email notifications, etc. are completely customizable
- Ticket history, evolution of ticket status and actions taken on ticket 
  are logged
- Completely Scalable - manage customer support inquiries, track bugs or manage 
  to-do lists
- Cross-Platform, Web-based Trouble Ticket Software
- Full search of archives (up to 2 years)
- On-demand custom reports (24 hour turnaround on custom report development)
- All communications are fully secured with 128-bit encryption
- Public API for easy integration with any other platform
- and much more...
